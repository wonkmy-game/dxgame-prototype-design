using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodMgr : MonoBehaviour,IDXLoder
{
    private Transform blood;

    public int totalBlood;
    private int curBlood;
    public void Init()
    {
        blood = transform.Find("blood");
    }

    public void SetBlood(int bloodValue)
    {
        totalBlood = bloodValue;
        curBlood = totalBlood;

        UpgradeBloodSlider();
    }

    public void DoDamage(int damage,out int _curBlood)
    {
        _curBlood = 0;
        curBlood -= damage;
        _curBlood = curBlood;
        UpgradeBloodSlider();
        if (curBlood <= 0)
        {
            curBlood = 0;
            UpgradeBloodSlider();
        }
    }

    void UpgradeBloodSlider()
    {
        blood.localScale = new Vector3((float)curBlood / totalBlood, 1, 1);
    }
}
