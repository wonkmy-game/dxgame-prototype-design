using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DXCameraFollow : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;
    public float speed;
    public void SetTarget(Transform _target)
    {
        target = _target;
    }
    private void LateUpdate()
    {
        if (target != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position + offset, speed * Time.deltaTime);
        }
    }
}
