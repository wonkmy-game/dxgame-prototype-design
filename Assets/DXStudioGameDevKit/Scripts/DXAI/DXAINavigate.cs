using System;
using System.Collections.Generic;
using UnityEngine;

public class DXPoint
{
    public int x;
    public int y;
    public int G = 0;            // G = the movement cost from the start point A to a given square on the grid, following the path generated to get there.
    public int H = 0;            // H = the estimated movement cost from that square on the grid to the final destination B.
    public int F = 0;            // F = G + H
    public DXPoint father = null;  // The previous point of this point, which can be used to trace back to the starting point.
    public bool is_close = false; // Whether the point is closed during the search.

    public override string ToString()
    {
        return x + ":" + y;
    }
}

public class DXAINavigate
{
    public static DXPoint start = null;      // Start point
    public static DXPoint end = null;        // End point
    public static Dictionary<int, DXPoint> map = null;   // Map points
    public static Vector2Int size = Vector2Int.zero;    // Map size

    public static List<DXPoint> arr_open = new List<DXPoint>();  // Open queue
    public static DXPoint pppp = null;       // After the pathfinding is complete, it will have a value unless no path is found.

    /**
     * Get the route (This pathfinding does not allow diagonal movement)
     */
    public static List<DXPoint> GetRoute(DXPoint start, DXPoint end, Dictionary<int, DXPoint> map, Vector2Int size)
    {
        is_find = false;
        arr_open.Clear();
        pppp = null;
        DXAINavigate.start = new DXPoint { x = start.x, y = start.y };
        DXAINavigate.end = new DXPoint { x = end.x, y = end.y };
        DXAINavigate.map = new Dictionary<int, DXPoint>(map);
        DXAINavigate.size = size;
        map[start.x + start.y * size.x].G = 0;

        // Start pathfinding
        List<DXPoint> route = new List<DXPoint>();
        try
        {
            Search(DXAINavigate.start);
        }
        catch (Exception error)
        {
            Debug.LogError("Position is incorrect: " + error.Message);
            return route;
        }
        if (pppp != null)
        {
            GetFather(pppp, route);
        }
        return route;
    }

    /**
     * Pathfinding
     */
    public static bool is_find = false;    // Whether the route has been found
    static void Search(DXPoint point)
    {
        if (point.x == end.x && point.y == end.y)
        {
            is_find = true;
            pppp = point;
            return;
        }
        List<DXPoint> arr = GetAround(point);
        foreach (DXPoint p in arr)
        {
            SetFather(p, point);
        }
        // Sort arr by F value from smallest to largest
        arr_open.Sort((p1, p2) => p1.F.CompareTo(p2.F));
        // Recursively continue searching
        foreach (DXPoint pp in new List<DXPoint>(arr_open))
        {
            if (pp.is_close)
            {
                arr_open.Remove(pp);
            }
            if (!is_find)
            {
                Search(pp);
            }
        }
    }

    /**
     * Get the 4 surrounding points, up, down, left, and right
     */
    static List<DXPoint> GetAround(DXPoint point)
    {
        point.is_close = true;
        List<DXPoint> arr = new List<DXPoint>();
        DXPoint p;
        // Up
        if (point.y != 0)
        {
            int index = point.x + (point.y - 1) * size.x;
            if (map.TryGetValue(index, out p) && !p.is_close)
            {
                arr.Add(map[index]);
                arr_open.Add(map[index]);
            }
        }
        // Down
        if (point.y + 1 != size.y)
        {
            int index = point.x + (point.y + 1) * size.x;
            if (map.TryGetValue(index, out p) && !p.is_close)
            {
                arr.Add(map[index]);
                arr_open.Add(map[index]);
            }
        }
        // Left
        if (point.x != 0)
        {
            int index = point.x - 1 + point.y * size.x;
            if (map.TryGetValue(index, out p) && !p.is_close)
            {
                arr.Add(map[index]);
                arr_open.Add(map[index]);
            }
        }
        // Right
        if (point.x + 1 != size.x)
        {
            int index = point.x + 1 + point.y * size.x;
            if (map.TryGetValue(index, out p) && !p.is_close)
            {
                arr.Add(map[index]);
                arr_open.Add(map[index]);
            }
        }
        return arr;
    }

    /**
     * Set the father point of the given point and recalculate G, H, F
     */
    static void SetFather(DXPoint son, DXPoint father)
    {
        if (son.father == null || son.father.G > father.G)
        {
            son.father = father;
            son.G = son.father.G + 1;
            son.H = Mathf.Abs(son.x - end.x) + Mathf.Abs(son.y - end.y);
            son.F = son.G + son.H;
        }
    }

    /**
     * Recursive function to put ancestors into the route
     */
    static void GetFather(DXPoint point, List<DXPoint> route)
    {
        DXPoint father = point.father;
        if (father != null)
        {
            GetFather(father, route);
        }
        route.Add(point);
    }
}
