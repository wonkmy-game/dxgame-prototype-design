using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;

public class DXUtils : MonoBehaviour, IDXLoder
{
    public static DXUtils instance;

    public void Init()
    {
        instance = this;
    }
    public void DelayTodo(float delaytime, UnityAction callBack)
    {
        StartCoroutine(DelayToDo(delaytime, callBack));
    }
    IEnumerator DelayToDo(float delaytime,UnityAction callBack)
    {
        yield return new WaitForSeconds(delaytime);
        callBack();
    }
    public static string CreateNewFolder(string newFolderName)
    {
        string path1 = Application.dataPath + "/GameDatas/";
        string folderPath = Path.Combine(path1, newFolderName);
        if (!Directory.Exists(folderPath))
        {
            Directory.CreateDirectory(folderPath);
        }
        return folderPath;
    }
    public static void WriteFile(string filePath,string content, bool isOverride, bool needEncroy = false)
    {
        if (isOverride)
        {
            using (StreamWriter sw = new StreamWriter(filePath, false))
            {
                if (needEncroy)
                {
                    string encryptedContent = EncryptString(content);
                    sw.WriteLine(encryptedContent);
                }
                else
                {
                    sw.WriteLine(content);
                }
            }
        }
        else
        {
            using (StreamWriter sw = File.AppendText(filePath))
            {
                if (needEncroy)
                {
                    string encryptedJson = EncryptString(content);
                    sw.WriteLine(encryptedJson);
                }
                else
                {
                    sw.WriteLine(content);
                }
            }
        }
    }
    public static void ReadFile(string filePath, bool needEncroy = false, ParseFileCallback callback = null) {
        // 读取整个文件内容为一个字符串
        string fileContent = File.ReadAllText(filePath);
        if (needEncroy)
        {
            string content = DecryptString(fileContent);
            StringReader reader = new StringReader(content);

            while (reader.Peek() != -1)
            {
                string lineData = reader.ReadLine();
                callback(lineData);
            }
            reader.Close();
        }
        else
        {
            StringReader reader = new StringReader(fileContent);

            while (reader.Peek() != -1)
            {
                string lineData = reader.ReadLine();
                callback(lineData);
            }
            reader.Close();
        }
    }
    /// <summary>
    /// 读取txt文本文件
    /// </summary>
    /// <param name="filePath">文本文件相对于Resources文件夹的路径+名称</param>
    /// <param name="separator">文本文件中的每一个数据使用什么字符分割的</param>
    /// <param name="dataCount">每条数据共有几个属性</param>
    /// <param name="callback">回调函数用来处理拿到数据之后的处理</param>
    public static void ReadFile(string filePath,char separator,int dataCount, ParseFileDataCallback callback)
    {
        TextAsset textAsset = Resources.Load<TextAsset>(filePath);
        StringReader reader = new StringReader(textAsset.text);

        while (reader.Peek() != -1)
        {
            string lineData = reader.ReadLine();

            string[] values = lineData.Split(separator);
            if (values.Length >= dataCount)
            {
                callback(values);
            }
            else
            {
                Debug.LogError("Invalid data format");
            }
        }
        reader.Close();
    }

    public List<Vector3> GetPositionList(Vector3 startPos,float[] ringDistance,int[] ringPos)
    {
        List<Vector3> posList = new List<Vector3>();
        posList.Add(startPos);
        for (int i = 0; i < ringDistance.Length; i++)
        {
            posList.AddRange(GetPositionList(startPos, ringDistance[i], ringPos[i]));
        }
        return posList;
    }
    public List<Vector3> GetPositionList(Vector3 startPos,float distance,int count)
    {
        List<Vector3> posList = new List<Vector3>();
        for (int i = 0; i < count; i++)
        {
            var radians = i * (360f / count);
            Vector3 dir = ApplyRot(new Vector3(1, 0), radians);
            Vector3 position = startPos + dir * distance;
            posList.Add(position);
        }
        return posList;
    }

    Vector3 ApplyRot(Vector3 vec,float angle)
    {
        return Quaternion.Euler(0, 0, angle) * vec;
    }
    public List<Vector3> GetCiclePostion(Vector3 center, int count, float min = 2, float max = 3)
    {
        var radians = (Mathf.PI / 180) * Mathf.Round(360.0f / count);
        List<Vector3> posList = new List<Vector3>();

        for (int i = 0; i < count; i++)
        {
            float x = center.x + UnityEngine.Random.Range(min, max) * Mathf.Sin(radians * i);
            float y = center.y + UnityEngine.Random.Range(min, max) * Mathf.Cos(radians * i);
            posList.Add(new Vector3(x, y, 0));
        }
        return posList;
    }
    /// <summary>
    /// 根据概率数组选择随机的id
    /// </summary>
    /// <param name="probabilities"></param>
    /// <returns></returns>
    public static int GetRandom(float[] probabilities)
    {
        // 计算概率数组的总和
        float total = 0;
        foreach (float prob in probabilities)
        {
            total += prob;
        }

        // 生成随机概率值
        float randomPoint = UnityEngine.Random.value * total;

        // 根据随机概率值选择type
        for (int i = 0; i < probabilities.Length; i++)
        {
            if (randomPoint < probabilities[i])
            {
                return i;
            }
            else
            {
                randomPoint -= probabilities[i];
            }
        }
        return 0;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="rate">几率数组%</param>
    /// <param name="total">几率总和</param>
    /// <returns></returns>
    public static int GetRandom(int[] rate,int total)
    {
        int r = UnityEngine.Random.Range(1, total + 1);
        int t = 0;

        for (int i = 0; i < rate.Length; i++)
        {
            t += rate[i];
            if (r < t)
            {
                return i;
            }
        }
        return 0;
    }
    public static int[] ExtractTwoNumbers(string input)
    {
        int[] range = new int[2];
        // 定义匹配数字的正则表达式
        Regex regex = new Regex(@"\d+");
        // 使用正则表达式找到所有匹配项
        MatchCollection matches = regex.Matches(input);

        // 确保我们找到了两个数字
        if (matches.Count >= 2)
        {
            int firstNumber = int.Parse(matches[0].Value);
            int secondNumber = int.Parse(matches[1].Value);
            range[0] = firstNumber;
            range[0] = secondNumber;
            return range;
        }
        // 如果未找到两个数字，返回 null
        return null;
    }

    #region 将数据写入json文件并可以加密解密
    public static void WriteMapDataToJsonFile<T>(T data, string filePath, bool needEncry = false)
    {
        // 将 MapData 对象转换为 JSON 字符串
        string json = JsonUtility.ToJson(data);

        if (needEncry == true)
        {
            string encryptedJson = EncryptString(json);
            // 将 JSON 字符串写入文件
            File.WriteAllText(filePath, encryptedJson);
        }
        else
        {
            // 将 JSON 字符串写入文件
            File.WriteAllText(filePath, json);
        }

#if UNITY_EDITOR
        Debug.Log("Map data written to file: " + filePath);
#endif
    }
    public static bool IsHaveLocalStorage(string filePath)
    {
        return File.Exists(filePath);
    }
    public static T ReadMapDataFromJsonFile<T>(string filePath,bool needEncry = false)
    {
        // 检查文件是否存在
        if (File.Exists(filePath))
        {
            if (needEncry == true)
            {
                string encryptedJson = File.ReadAllText(filePath);
                string json = DecryptString(encryptedJson);
                T mapData = JsonUtility.FromJson<T>(json);
                return mapData;
            }
            else if (needEncry == false)
            {
                // 读取文件中的JSON字符串
                string json = File.ReadAllText(filePath);
                T mapData = JsonUtility.FromJson<T>(json);
                return mapData;
            }
            else
            {
                return default;
            }
        }
        else
        {
            Debug.LogError("File does not exist at path: " + filePath);
            return default;
        }
    }
    private static string EncryptString(string input)
    {
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(input);
        return Convert.ToBase64String(bytes);
    }

    // 简单的解密算法（Base64解码）
    private static string DecryptString(string input)
    {
        byte[] bytes = Convert.FromBase64String(input);
        return System.Text.Encoding.UTF8.GetString(bytes);
    }
    #endregion

    public static T GetEnumFromString<T>(string str)
    {
        return (T)System.Enum.Parse(typeof(T), str);
    }
    public static Sprite CreateSprite(int w=90,int h=90)
    {
        Texture2D tex = new Texture2D(w, h, TextureFormat.RGBA32, false);
        // 填充纹理为灰色
        Color grayColor = Color.gray;
        Color[] pixels = new Color[w * h];
        for (int i = 0; i < pixels.Length; i++)
        {
            pixels[i] = grayColor;
        }
        tex.SetPixels(pixels);
        tex.Apply();

        return Sprite.Create(tex, new Rect(0, 0, w, h), Vector2.one / 2.0f);
    }
    public static Sprite CreateSpriteFromFile(string path)
    {
        Texture2D tex = Resources.Load<Texture2D>(path);
        return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.one / 2.0f);
    }

    public static void AddAplhaTransition(UnityAction callBack,float endValue,float timer,float delay)
    {
        GameManager.instance.cicle_transition.GetChild(0).transform.DOScale(endValue, timer).SetDelay(delay).OnComplete(() => { callBack(); });
    }
}

public delegate void ParseFileDataCallback(string[] strList);
public delegate void ParseFileCallback(string str);
public delegate void OnFlyTextFinishedFlying(int num);