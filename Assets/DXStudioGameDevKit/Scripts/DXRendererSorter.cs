﻿using UnityEngine;

/*
    * 这是一个按照y值自动进行遮挡的脚本，特别适合那种2d的游戏，需要根据y坐标来进行遮挡效果的
    * */
public class DXRendererSorter : MonoBehaviour
{

    [SerializeField] private int sortingOrderBase = 5000;
    [SerializeField] private int offset = 0;
    [SerializeField] private bool runOnlyOnce = false;

    private float timer;
    private float timerMax = .1f;
    private Renderer myRenderer;

    private void Awake()
    {
        myRenderer = gameObject.GetComponent<Renderer>();
    }

    private void LateUpdate()
    {
        timer -= Time.deltaTime;
        if (timer <= 0f)
        {
            timer = timerMax;
            myRenderer.sortingOrder = (int)(sortingOrderBase - transform.position.y - offset);
            if (runOnlyOnce)
            {
                Destroy(this);
            }
        }
    }

    public void SetOffset(int offset)
    {
        this.offset = offset;
    }

}