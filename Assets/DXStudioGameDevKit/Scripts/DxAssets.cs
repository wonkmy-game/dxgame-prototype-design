﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DxAssets : MonoBehaviour
{

    // Internal instance reference
    private static DxAssets _i;

    // Instance reference
    public static DxAssets i
    {
        get
        {
            if (_i == null) _i = Instantiate(Resources.Load<DxAssets>("CodeMonkeyAssets"));
            return _i;
        }
    }


    // All references

    public Sprite s_White;
    public Sprite s_Circle;

    public Material m_White;

}
