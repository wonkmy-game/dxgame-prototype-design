using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
//这是滑动屏幕事件管理器，直接挂载在UI.Slider上即可，在你需要进行滑动监测的地方直接将下方三个UnityAction事件绑定逻辑即可: touchSlider.OnPointerDownEvent += OnPointerDown;
public class DXTouchSlider : MonoBehaviour, IPointerDownHandler, IPointerUpHandler,IDXLoder
{
    public UnityAction OnPointerDownEvent;
    public UnityAction<float> OnPointerDragEvent;
    public UnityAction OnPointerUpEvent;

    private Slider uiSlider;
    public void Init()
    {
        uiSlider = GetComponent<Slider>();
        uiSlider.onValueChanged.AddListener(OnSliderValueChanged);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (OnPointerDownEvent != null)
            OnPointerDownEvent.Invoke();

        if (OnPointerDragEvent != null)
            OnPointerDragEvent.Invoke(uiSlider.value);
    }

    private void OnSliderValueChanged(float value)
    {
        if (OnPointerDragEvent != null)
            OnPointerDragEvent.Invoke(value);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (OnPointerUpEvent != null)
            OnPointerUpEvent.Invoke();

        // reset slider value:
    }

    private void OnDestroy()
    {
        //remove listeners: to avoid memory leaks
        uiSlider.onValueChanged.RemoveListener(OnSliderValueChanged);
    }

   
}