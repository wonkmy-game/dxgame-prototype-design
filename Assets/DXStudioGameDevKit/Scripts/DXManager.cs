using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
/// <summary>
/// Author:独行社游戏工作室
/// 脚本名:DXManager
/// 功能:开发套件loader启动管理器
/// </summary>
public class DXManager : MonoBehaviour
{
    public void Start()
    {
        GameObject utils = new GameObject("utils");

        utils.transform.SetParent(transform);
        utils.AddComponent<DXUtils>().Init();

        GameObject localStorage = new GameObject("LocalStorage");
        localStorage.transform.SetParent(transform);
        localStorage.AddComponent<DXLocalStorage>().Init();

        transform.GetComponentInChildren<DXAudioManager>().Init();

        string folderPath = Path.Combine(Application.dataPath, "GameDatas");
        if (!Directory.Exists(folderPath))
        {
            Directory.CreateDirectory(folderPath);
            Debug.Log("Folder created at: " + folderPath);
        }

        FindObjectOfType<GameManager>().Init();
    }
}

public interface IDXLoder {
    void Init();
}

/// <summary>
/// 简单泛型工厂类
/// </summary>
/// <typeparam name="T"></typeparam>
public class DXLoaderFactory<T>
{
    public static T Create<K>()
    {
        return (T)Activator.CreateInstance(typeof(K));
    }
}

public delegate void OnGetLocalStorageFile(string data);