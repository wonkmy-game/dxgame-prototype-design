﻿using System;
using UnityEngine;


/// <summary>
/// 各种debug工具，例如在世界位置创建一个文本或者创建一个飘字
/// </summary>
public static class DXDebug
{
    // Creates a World Text object at the world position
    public static void Text(string text, Vector3 localPosition = default(Vector3), Transform parent = null, float fontSize = 40, Color? color = null, TextAnchor textAnchor = TextAnchor.UpperLeft, TextAlignment textAlignment = TextAlignment.Left, int sortingOrder = DXUtilsStatic.sortingOrderDefault)
    {
        DXUtilsStatic.CreateWorldText(text, parent, localPosition, fontSize, color, textAnchor, textAlignment, sortingOrder);
    }

    // World text pop up at mouse position
    public static void TextPopupMouse(string text)
    {
        DXUtilsStatic.CreateWorldTextPopup(text, DXUtilsStatic.GetMouseWorldPosition());
    }

    /// <summary>
    /// Creates a Text pop up at the world position
    /// </summary>
    /// <param name="text"></param>
    /// <param name="position"></param>
    public static void TextPopup(string text, Vector3 position)
    {
        DXUtilsStatic.CreateWorldTextPopup(text, position);
    }

    // Text Updater in World, (parent == null) = world position
    public static FunctionUpdater TextUpdater(Func<string> GetTextFunc, Vector3 localPosition, Transform parent = null)
    {
        return DXUtilsStatic.CreateWorldTextUpdater(GetTextFunc, localPosition, parent);
    }

    // Text Updater in UI
    public static FunctionUpdater TextUpdaterUI(Func<string> GetTextFunc, Vector2 anchoredPosition)
    {
        return DXUtilsStatic.CreateUITextUpdater(GetTextFunc, anchoredPosition);
    }

    // Text Updater always following mouse
    public static void MouseTextUpdater(Func<string> GetTextFunc, Vector3 positionOffset = default(Vector3))
    {
        GameObject gameObject = new GameObject();
        FunctionUpdater.Create(() => {
            gameObject.transform.position = DXUtilsStatic.GetMouseWorldPosition() + positionOffset;
            return false;
        });
        TextUpdater(GetTextFunc, Vector3.zero, gameObject.transform);
    }

    // Trigger Action on Key
    public static FunctionUpdater KeyCodeAction(KeyCode keyCode, Action onKeyDown)
    {
        return DXUtilsStatic.CreateKeyCodeAction(keyCode, onKeyDown);
    }



    // Debug DrawLine to draw a projectile, turn Gizmos On
    public static void DebugProjectile(Vector3 from, Vector3 to, float speed, float projectileSize)
    {
        Vector3 dir = (to - from).normalized;
        Vector3 pos = from;
        FunctionUpdater.Create(() => {
            Debug.DrawLine(pos, pos + dir * projectileSize);
            float distanceBefore = Vector3.Distance(pos, to);
            pos += dir * speed * Time.deltaTime;
            float distanceAfter = Vector3.Distance(pos, to);
            if (distanceBefore < distanceAfter)
            {
                return true;
            }
            return false;
        });
    }


}
