using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class InventoryPanel//相当于背包的显示层
{
    private Inventory inventoryData;
    private GridLayoutGroup layoutGroup;
    private GameObject inventoryPanelEntity;
    Vector2 itemHV;
    public InventoryPanel(Inventory data,Transform parent,Vector2 _itemHV = default)
    {
        itemHV = _itemHV;
        inventoryData = data;
        inventoryData.data = data.data;
        inventoryPanelEntity = Object.Instantiate(Resources.Load<GameObject>("BagSystem/InventoryPanel"));
        layoutGroup = inventoryPanelEntity.GetComponent<GridLayoutGroup>();
        layoutGroup.cellSize = _itemHV;
        inventoryPanelEntity.transform.SetParent(parent);
        int perLineCount = CalculateSlotsPerRow(data.TotalCapcity);
        int width = (perLineCount * (int)itemHV.x) + Mathf.FloorToInt((perLineCount - 1) * layoutGroup.spacing.x) + (layoutGroup.padding.left) * 2;
        int lineCount = Mathf.FloorToInt((float)data.TotalCapcity / perLineCount);//5
        int height = (lineCount * (int)itemHV.y) + Mathf.FloorToInt((lineCount - 1) * layoutGroup.spacing.y) + (layoutGroup.padding.top) * 2;
        SetSizeAndPos(inventoryPanelEntity.GetComponent<RectTransform>(), width, height);
    }

    void SetSizeAndPos(RectTransform rt, float width, float height)
    {
        Vector2 oldAnchorMin = rt.anchorMin;
        Vector2 oldAnchorMax = rt.anchorMax;

        //rt.anchorMin = new Vector2(0, 0);
        //rt.anchorMax = new Vector2(0, 0);

        rt.sizeDelta = new Vector2(width, height);

        //rt.anchorMin = oldAnchorMin;
        //rt.anchorMax = oldAnchorMax;

        rt.anchoredPosition = Vector3.zero;

        rt.localScale = Vector3.one;
        
    }
    int CalculateSlotsPerRow(int capacity)
    {
        // 定义每行格子数量的规则
        if (capacity <= 8)
        {
            return 4;
        }
        else if (capacity <= 50)
        {
            return 5;
        }
        else if (capacity <= 100)
        {
            return 9;
        }
        else
        {
            return 11;
        }
    }
    public void UpdateData(Vector3Int coord = default,bool noOffset=true)
    {
        if (noOffset)
        {
            inventoryPanelEntity.transform.localPosition = Vector3.zero;
        }
        else
        {
            inventoryPanelEntity.transform.localPosition = new Vector3(143 * coord.x, 143 * (coord.y + 1));
        }
        for (int i = 0; i < inventoryData.TotalCapcity; i++)
        {
            GameObject newItemCellbg = Object.Instantiate(Resources.Load<GameObject>("BagSystem/itemCellbg"));
            newItemCellbg.GetComponent<RectTransform>().sizeDelta = new Vector2(itemHV.x, itemHV.y);
            newItemCellbg.transform.SetParent(inventoryPanelEntity.transform);
            newItemCellbg.transform.localScale = Vector3.one;
        }

        for (int i = 0; i < inventoryData.data.Count; i++)
        {
            GameObject newItemCell = Object.Instantiate(Resources.Load<GameObject>("BagSystem/" + (noOffset == true ? "Card_inventory" : "itemCell")));
            newItemCell.transform.SetParent(inventoryPanelEntity.transform.GetChild(i));
            newItemCell.GetComponent<RectTransform>().sizeDelta = new Vector2(itemHV.x, itemHV.y);
            newItemCell.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
            newItemCell.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
            newItemCell.transform.localPosition = Vector3.zero;
            newItemCell.transform.localScale = Vector3.one;
            Sprite sp = DXUtils.CreateSprite();

            if(noOffset == true)
            {
                newItemCell.GetComponent<CardCellInvent>().Init(inventoryData.data[i]);
            }
            else
            {
                newItemCell.GetComponent<ItemCell>().Init(sp, inventoryData.data[i].name, inventoryData.data[i].curCount, inventoryData.data[i].description);
            }
        }
    }

    public void DeleteAllGUI()
    {
        if (inventoryPanelEntity.transform.childCount > 0)
        {
            for (int i = 0; i < inventoryPanelEntity.transform.childCount; i++)
            {
                Object.Destroy(inventoryPanelEntity.transform.GetChild(i).gameObject);
            }
        }
    }
}
