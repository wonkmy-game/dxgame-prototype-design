using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class ItemCell : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler
{
    public Image itemIcon;
    public Text itemName;
    public Text itemCount;
    public Text itemDes;
    public void Init(Sprite itemIcon, string itemName, int itemCount,string itemDes)
    {
        this.itemIcon.sprite = itemIcon;
        this.itemName.text = itemName;
        this.itemCount.text = itemCount.ToString();
        this.itemDes.text = itemDes;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        itemDes.GetComponent<CanvasGroup>().alpha = 1;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        itemDes.GetComponent<CanvasGroup>().alpha = 0;
    }
}
