using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 创建新背包/容器的工厂类
/// </summary>
public class InventoryFactory
{
    public static Inventory CreateInventory(string invenName,int capcity)
    {
        Inventory inventory = new Inventory();
        inventory.TotalCapcity = capcity;
        inventory.name = invenName;
        inventory.Init();
        return inventory;
    }
}

[System.Serializable]
public class Inventory//相当于背包的数据层
{
    public int TotalCapcity;
    public string name;
    public List<Card> data;
    public void AddItem(Card item, bool needSave = true)
    {
        if (data.Count >= TotalCapcity) return;
        bool itemExists = false;

        for (int i = 0; i < data.Count; i++)
        {
            if (item.id == data[i].id)
            {
                if(data[i].IsFull == false)//当前item没有满
                {
                    data[i].curCount += item.curCount;
                    if (data[i].curCount > data[i].totalCount)
                    {
                        int excessCount = data[i].curCount - data[i].totalCount;
                        data[i].curCount = data[i].totalCount;
                        data[i].IsFull = true;
                        // 创建一个新的物品实例，将超出部分添加为新物品
                        Card newItem = new Card(data[i].id, data[i].name,data[i].level, data[i].description, excessCount, data[i].targetValue);
                        data.Add(newItem);
                        item = newItem;
                    }
                }
                itemExists = true;
            }
        }

        if (!itemExists)
        {
            data.Add(item);
        }
        if (needSave == true)
        {
            GameManager.instance.SaveGame();
        }
    }
    public void RemoveItem(Card c, CardCellInvent obj)
    {
        for (int i = 0; i < data.Count; i++) {
            if (c.id == data[i].id)
            {
                if (c.curCount > 1)
                {
                    c.curCount -= 1;
                    obj.UpdataData(c);
                }
                else
                {
                    data.Remove(c);
                    RemoveCardFromBagGUI(obj);
                }

                int resultGold = Random.Range(c.targetValue[0], c.targetValue[1]);
                FlashCards.instance.GoldValue += resultGold;
                FlashCards.instance.OnRewardGold(resultGold);
                break;
            }
        }
    }
    void RemoveCardFromBagGUI(CardCellInvent obj)
    {
        Object.Destroy(obj.gameObject);
    }
    public void Init()
    {
        data = new List<Card>();
    }
}



[System.Serializable]
public class Card
{
    public int id;
    public string name;
    public string level;
    public string description;
    public int curCount;
    public int totalCount;
    public int[] targetValue;

    public bool IsFull;
    public Card(int id, string name, string level, string description, int curCount, int[] _targetValue)
    {
        this.id = id;
        this.name = name;
        this.level = level;
        this.description = description;
        this.curCount = curCount;
        this.totalCount = 64;
        this.targetValue = _targetValue;
    }
}