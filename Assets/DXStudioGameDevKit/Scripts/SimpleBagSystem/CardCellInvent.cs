using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CardCellInvent : MonoBehaviour, IPointerEnterHandler,IPointerClickHandler
{
    public Text title;
    public Text level;
    public Text des;
    public Text count;

    Card cardData;
    public void Init(Card c)
    {
        cardData = c;
        title.text = c.name;
        level.text = c.level;
        des.text = c.description;
        count.text = "X "+ c.curCount.ToString() ;
    }

    public void UpdataData(Card c)
    {
        Init(c);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            FlashCards.instance.RemoveCardFromBag(this.cardData,this);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {

    }
}
