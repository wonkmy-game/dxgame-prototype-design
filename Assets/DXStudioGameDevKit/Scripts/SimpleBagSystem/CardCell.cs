using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class CardCell : MonoBehaviour,IDragHandler,IBeginDragHandler,IEndDragHandler,IPointerClickHandler, IPointerMoveHandler,IPointerExitHandler
{
    public Text title;
    public Text level;
    public Text des;

    RectTransform rectTransform;
    public Transform bg;
    private Vector2 previousPosition;

    // 用于调整旋转敏感度的因子
    [SerializeField] private float rotationSpeed = 0.2f;
    // 旋转角度限制
    [SerializeField]private float maxRotationAngle = 15f;

    public bool canRot = false;

    Card cardData;
    public void Init(Card c)
    {
        cardData = c;
        title.text = c.name;
        level.text = c.level;
        des.text = c.description;
        rectTransform = GetComponent<RectTransform>();
        canRot = false;
        rotationSpeed = 0.2f;
        maxRotationAngle = 15f;
    }
    
    public void OnBeginDrag(PointerEventData eventData)
    {
        rectTransform.localRotation = Quaternion.identity;
        previousPosition = eventData.position;
        canRot = false;
    }
    public void OnDrag(PointerEventData eventData)
    {
         // 获取当前鼠标在屏幕上的位置
        Vector2 currentPosition = eventData.position;
        // 计算鼠标移动的差值
        Vector2 delta = currentPosition - previousPosition;
        previousPosition = currentPosition;
        float xOffset = currentPosition.x < Screen.width / 2 ? -1 : 1;
        if (xOffset == -1)
        {
            Camera.main.DOColor(DXUtilsStatic.GetColorFromString("A5BC4F"), 1);
        }
        else
        {
            Camera.main.DOColor(DXUtilsStatic.GetColorFromString("BC5A4F"), 1);
        }
        Vector2 newEffDis = Vector2.Lerp(bg.GetComponent<Shadow>().effectDistance, new Vector2(delta.x * 60, 60 * delta.y), 1.5f * Time.deltaTime);
        bg.GetComponent<Shadow>().effectDistance = newEffDis;
        rectTransform.anchoredPosition += delta;
    }
    private float NormalizeAngle(float angle)
    {
        while (angle > 180) angle -= 360;
        while (angle < -180) angle += 360;
        return angle;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        // 获取当前UI元素的RectTransform组件
        rectTransform = GetComponent<RectTransform>();
        rectTransform.localRotation = Quaternion.identity;

        Vector2 currentPosition = eventData.position;
        // 计算鼠标移动的差值
        Vector2 delta = currentPosition - previousPosition;
        float xOffset = currentPosition.x < Screen.width / 2 ? -1 : 1;

        rectTransform.DOAnchorPosX(1440 * xOffset, 0.5f).OnComplete(()=> {
            //int resultGold = Random.Range(cardData.targetValue[0], cardData.targetValue[1]);
            //FlashCards.instance.GoldValue += resultGold;
            //FlashCards.instance.OnRewardGold(resultGold);

            if (xOffset == -1)
            {
                FlashCards.instance.AddCardToBag(new Card(cardData.id, cardData.name, cardData.level, cardData.description, 1, cardData.targetValue));
            }

            Destroy(gameObject);
        });
    }

    public void OnPointerMove(PointerEventData eventData)
    {
        if (canRot)
        {
            // 获取当前鼠标在屏幕上的位置
            Vector2 currentPosition = eventData.position;

            // 计算鼠标移动的差值
            Vector2 delta = currentPosition - previousPosition;

            // 更新 previousPosition 为当前位置，以便下次计算
            previousPosition = currentPosition;

            // 计算旋转角度
            float angleX = delta.y * rotationSpeed; // 上下移动影响绕X轴旋转
            float angleY = -delta.x * rotationSpeed; // 左右移动影响绕Y轴旋转，取负号是为了方向一致

            // 获取当前旋转的欧拉角度
            Vector3 currentEulerAngles = rectTransform.localRotation.eulerAngles;

            // 将欧拉角度转换为-180到180范围内
            currentEulerAngles.x = NormalizeAngle(currentEulerAngles.x);
            currentEulerAngles.y = NormalizeAngle(currentEulerAngles.y);

            // 计算新的旋转角度，限制在-30到30度之间
            float newAngleX = Mathf.Clamp(currentEulerAngles.x + angleX, -maxRotationAngle, maxRotationAngle);
            float newAngleY = Mathf.Clamp(currentEulerAngles.y + angleY, -maxRotationAngle, maxRotationAngle);

            // 应用新的旋转
            rectTransform.localRotation = Quaternion.Euler(newAngleX, newAngleY, 0);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        rectTransform.localRotation = Quaternion.identity;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            previousPosition = eventData.position;
        }
    }
}
