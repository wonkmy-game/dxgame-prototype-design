using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour
{
    public LineRenderer line;
    private Vector3 lastPos;

    List<Vector2> allPoints;

    private void Start()
    {
        // │§╩╝╗»LineRenderer
        if (line == null)
        {
            line = gameObject.AddComponent<LineRenderer>();
            line.startWidth = 10;
            line.endWidth = 10;
        }

        allPoints = new List<Vector2>();

        line.positionCount = 0;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mPos.z = 0;
            lastPos = mPos;
            Destroy(line.GetComponent<EdgeCollider2D>());
            if (allPoints.Count > 0)
            {
                allPoints.Clear();
            }
            line.positionCount = 1;
            line.SetPosition(0, lastPos);
            allPoints.Add(lastPos);
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mPos.z = 0;

            if (Vector3.Distance(mPos, lastPos) > 0.5f)
            {
                AddPoint(mPos);
                lastPos = mPos;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            line.gameObject.AddComponent<EdgeCollider2D>();
            line.GetComponent<EdgeCollider2D>().points = allPoints.ToArray();
        }
    }

    private void AddPoint(Vector3 point)
    {
        line.positionCount++;
        line.SetPosition(line.positionCount - 1, point);
        allPoints.Add(point) ;
    }
}
