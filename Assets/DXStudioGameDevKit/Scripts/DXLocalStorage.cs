using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DXLocalStorage : MonoBehaviour, IDXLoder
{
    public static DXLocalStorage instance;
    public void Init()
    {
        instance = this;
    }

    public void SaveGameData(string storageKey, string storageObj)
    {
        PlayerPrefs.SetString(storageKey, storageObj);
        PlayerPrefs.Save();
    }

    public void GetGameData(string storageKey, OnGetLocalStorageFile onGetLocalStorageFile) {
        if (PlayerPrefs.HasKey(storageKey))
        {
            string data = PlayerPrefs.GetString(storageKey);
            onGetLocalStorageFile(data);
        }
        else
        {
            Debug.Log("暂无存档");
            onGetLocalStorageFile("");
        }
    }
}


//以上只是实现了一个保存和读取存档的通用逻辑，具体存储的Object类型需要自行构建
//public struct GameData
//{
//  xxxx
//}
//类似上面的就行,然后在处理好数据后，转换成json字符串保存即可