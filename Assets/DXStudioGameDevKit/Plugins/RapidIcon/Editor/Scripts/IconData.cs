﻿using System;
using System.Collections.Generic;

namespace RapidIconUIC
{
	[Serializable]
	public class IconData
	{
		public List<Icon> icons;

		public IconData()
		{
			icons = new List<Icon>();
		}
	}
}