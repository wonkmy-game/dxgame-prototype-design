using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResEntity : MonoBehaviour
{
    public ResType resType;
    public bool CanBroke { get; set; }
    ResSpriteLoader resSpriteLoader;
    SpriteRenderer spriteRenderer;

    public Vector2Int coordinate;
    BloodMgr bloodMgr;

    public void Init(ResType _type, Vector2Int _coordinate,bool canBroke)
    {
        resSpriteLoader = GetComponent<ResSpriteLoader>();
        spriteRenderer = transform.GetChild(1).GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = resSpriteLoader.GetSprite();
        bloodMgr = GetComponentInChildren<BloodMgr>();
        bloodMgr.Init();
        bloodMgr.SetBlood(100);
        resType = _type;
        CanBroke = canBroke;
        bloodMgr.gameObject.SetActive(CanBroke);
        coordinate = _coordinate;
    }

    public void Damage(int num,OnResEntityDelete callBack)
    {
        if (CanBroke == true)
        {
            int curBlood = 0;
            bloodMgr.DoDamage(num, out curBlood);

            if (curBlood <= 0)
            {
                callBack();
            }
        }
    }
}
