using UnityEngine;
using System.Collections.Generic;

public class CharacterMovement : MonoBehaviour
{
    public float moveSpeed = 5f; // 移动速度
    private List<DXPoint> path = new List<DXPoint>(); // 路径点列表
    private int currentWaypointIndex = 0; // 当前路径点索引

    // 在开始时调用
    void Start()
    {
        // 在这里调用路径寻找方法并获取路径点列表
        DXPoint start = new DXPoint { x = 8, y = 4 }; // 设置起点
        DXPoint end = new DXPoint { x = 22, y = 20 };   // 设置终点
        Dictionary<int, DXPoint> map = CreateMap(); // 创建地图数据
        Vector2Int size = new Vector2Int(28, 27); // 设置地图尺寸
        path = DXAINavigate.GetRoute(start, end, map, size); // 获取路径点列表

        foreach (var p in map)
        {
            Vector3 moveDirection = new Vector3(p.Value.x, p.Value.y, 0);
            if (p.Value.is_close == true)
            {
                DXDebug.Text(p.Value.ToString(), moveDirection, null, 0.08f, Color.red);
            }
            else
            {
                DXDebug.Text(p.Value.ToString(), moveDirection, null, 0.08f,Color.white);
            }
        }

        foreach (var p in path)
        {
            Vector3 moveDirection = new Vector3(p.x, p.y, 0);
            DXDebug.Text(p.ToString(), moveDirection, null, 0.05f, Color.blue);
        }
    }

    // 在每一帧更新时调用
    void Update()
    {
        // 如果没有路径或者已经到达终点，则停止移动
        if (path.Count == 0 || currentWaypointIndex >= path.Count)
        {
            return;
        }

        // 获取当前路径点
        DXPoint targetWaypoint = path[currentWaypointIndex];

        Vector3 moveDirection = new Vector3(targetWaypoint.x - transform.position.x, targetWaypoint.y - transform.position.z, 0) + new Vector3(10, 0, 0);

        transform.position = Vector3.MoveTowards(transform.position, moveDirection, moveSpeed * Time.deltaTime);
        // 移动物体
        //transform.Translate(moveDirection * moveSpeed * Time.deltaTime, Space.World);

        // 如果已经接近当前路径点，则前往下一个路径点
        if (Vector3.Distance(transform.position, moveDirection) < 0.1f)
        {
            currentWaypointIndex++;
        }
    }

    // 创建地图数据的示例方法
    private Dictionary<int, DXPoint> CreateMap()
    {
        // 在这里创建地图数据，这是一个示例方法，你可以根据实际情况创建地图数据
        Dictionary<int, DXPoint> map = new Dictionary<int, DXPoint>();
        // 假设地图大小为10x10，所有点都可以通行
        for (int x = 0; x < 28; x++)
        {
            for (int y = 0; y < 27; y++)
            {
                DXPoint point = new DXPoint { x = x, y = y };
                // 判断当前点是否为不可通行点，这里假设将(3, 3)和(4, 3)两个点设置为不可通行
                if ((x == 3 && y == 3) || (x == 4 && y == 3) || (x == 0 && y == 3))
                {
                    // 设置不可通行点
                    point.is_close = true;
                }
                map.Add(x + y * 28, point);
            }
        }
        return map;
    }
}
