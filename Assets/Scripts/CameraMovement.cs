using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public float moveSpeed;
    void Update()
    {
        if (GameManager.instance.GameStart)
        {
            Vector3 inputVec = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            Vector3 targetPos = inputVec + transform.position;

            transform.position = Vector3.MoveTowards(transform.position, targetPos, moveSpeed * Time.deltaTime);

            if (transform.position.x <= -4.8f)
            {
                transform.position = new Vector3(-4.8f, transform.position.y, -10);
            }
            if (transform.position.x >= 4.8f)
            {
                transform.position = new Vector3(4.8f, transform.position.y, -10);
            }
            if (transform.position.y >= 2.3f)
            {
                transform.position = new Vector3(transform.position.x, 2.3f, -10);
            }
            if (transform.position.y <= -12.2f)
            {
                transform.position = new Vector3(transform.position.x, -12.2f, -10);
            }
        }
    }
}
