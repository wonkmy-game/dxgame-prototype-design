using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FileCell : MonoBehaviour
{
    public Text fileName_label;
    public Button load_btn;
    public void Init(string path, string fileName)
    {
        fileName_label.text = fileName;
        load_btn.onClick.AddListener(()=> {
            GameManager.instance.mapManager.filePath = path + "/" + fileName+".json";
            GameManager.instance.mapManager.StartGenerator("load_game");
            GameManager.instance.filePath = GameManager.instance.mapManager.filePath;
            GameManager.instance.curFolderName = fileName;

            GameManager.instance.SavePlayerResCount(new ResCountData(0,0,0,0));
        });
    }
}
