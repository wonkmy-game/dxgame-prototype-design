using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameingPanel : MonoBehaviour
{
    public Text wood_text;
    public Text stone_text;
    public Text gold_text;
    public Text food_text;

    public void UpgradeAllREsLabel(ResCountData resCountData)
    {
        wood_text.text = resCountData.Woods.ToString();
        stone_text.text = resCountData.Stones.ToString();
        gold_text.text = resCountData.Golds.ToString();
        food_text.text = resCountData.Foods.ToString();
    }
}
