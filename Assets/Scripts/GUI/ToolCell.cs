using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolCell : MonoBehaviour
{
    public ToolType toolType;
    public Image icon;

    public void Init(ToolType type,string iconPath)
    {
        Sprite sp = DXUtils.CreateSpriteFromFile(iconPath);
        icon.sprite = sp;
        toolType = type;
    }
}
