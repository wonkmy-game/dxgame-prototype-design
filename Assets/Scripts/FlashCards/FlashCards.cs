using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;
using UnityEngine.Events;
using System.Text.RegularExpressions;

public class FlashCards : MonoBehaviour
{
    public static FlashCards instance;

    public List<Card> cardList;

    public Button flash_card_btn;
    public Text gold_label;
    public Text add_gold_label;
    public Text cost_gold_label;

    int FlashCount = 0;

    int goldValue = 0;
    public int GoldValue
    {
        get
        {
            return goldValue;
        }
        set
        {
            goldValue = value;
            if (goldValue <= 0)
            {
                goldValue = 0;
            }
            gold_label.text = goldValue.ToString();
        }
    }
    int curLevel = 0;
    public int Level
    {
        get
        {
            return curLevel;
        }
        set
        {
            curLevel = value;
            cost_gold_label.text = (curLevel * 5).ToString();
        }
    }

    public Inventory cardBag;
    public InventoryPanel cardBagPanel;
    public Transform bagCellParent;

    Tweener scale;
    Tweener moveY;
    Tweener opacity;

    Sequence sequence;

    GameObject newCardCell;
    List<float[]> properList;

    int curSelectProIndex = 0;
    private void Awake()
    {
        instance = this;
        cardList = new List<Card>();
        properList = new List<float[]>();

        flash_card_btn.onClick.AddListener(OnFlashCards);
        GoldValue = 100;
        Level = 1;

        cardBag = InventoryFactory.CreateInventory("Mybag", 15);
        cardBagPanel = new InventoryPanel(cardBag, bagCellParent, new Vector2(450.0f / 2.0f, 600.0f / 2.0f));
    }
    private void Start()
    {
        DXUtils.ReadFile("cards", '#', 5, (data) => {
            string[] targetValue = data[4].Split('@');
            Card card = new Card(int.Parse(data[0]), data[1], data[2], data[3], 0, new int[] {int.Parse(targetValue[0]), int.Parse(targetValue[1]) });
            if (!cardList.Contains(card))
            {
                cardList.Add(card);
            }
        });

        //            从左到右分别是 D、  C、     B、   A、    S、    SS、  SSS   级别卡牌的出现概率
        float[] f1 = new float[] { 0.5f, 0.3f, 0.08f, 0.04f, 0.04f, 0.03f, 0.01f };
        float[] f2 = new float[] { 0.5f, 0.4f, 0.08f, 0.04f, 0.04f, 0.03f, 0.01f };
        float[] f3 = new float[] { 0.5f, 0.3f, 0.18f, 0.04f, 0.04f, 0.03f, 0.01f };
        float[] f4 = new float[] { 0.5f, 0.3f, 0.08f, 0.08f, 0.04f, 0.03f, 0.01f };
        float[] f5 = new float[] { 0.5f, 0.3f, 0.08f, 0.04f, 0.08f, 0.03f, 0.01f };
        float[] f6 = new float[] { 0.5f, 0.3f, 0.08f, 0.04f, 0.04f, 0.06f, 0.01f };
        float[] f7 = new float[] { 0.5f, 0.3f, 0.08f, 0.04f, 0.04f, 0.03f, 0.02f };

        properList.Add(f1);
        properList.Add(f2);
        properList.Add(f3);
        properList.Add(f4);
        properList.Add(f5);
        properList.Add(f6);
        properList.Add(f7);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (bagCellParent.gameObject.activeSelf)
            {
                cardBagPanel.DeleteAllGUI();
                bagCellParent.gameObject.SetActive(false);
            }
            else
            {
                bagCellParent.gameObject.SetActive(true);

                cardBagPanel.UpdateData(Vector3Int.zero,true);
            }
        }
    }
    public void AddCardToBag(Card c)
    {
        cardBag.AddItem(c,false);
    }
    public void RemoveCardFromBag(Card c, CardCellInvent cardObj)
    {
        cardBag.RemoveItem(c, cardObj);
    }
    public int GetLevelValueByLevelStr(string levelStr)
    {
        if (levelStr == "D")
        {
            return 0;
        }
        else if (levelStr == "C")
        {
            return 1;
        }
        else if (levelStr == "B")
        {
            return 2;
        }
        else if (levelStr == "A")
        {
            return 3;
        }
        else if (levelStr == "S")
        {
            return 4;
        }
        else if (levelStr == "SS")
        {
            return 5;
        }
        else if (levelStr == "SSS")
        {
            return 6;
        }
        else
        {
            return -1;
        }
    }
    void OnFlashCards()
    {
        if (GoldValue >= 5 * Level)
        {
            GoldValue -= 5 * Level;
            FlashCount++;
            if (FlashCount > 0 && FlashCount % 4 == 0)
            {
                Level += 1;
                GoldValue += Random.Range(10, 20);
            }
            if (FlashCount > 0 && FlashCount % 20 == 0)
            {
                curSelectProIndex = Random.Range(0, properList.Count);
            }
            if (newCardCell != null)
            {
                Destroy(newCardCell);
                newCardCell = null;
            }
            if (scale != null && scale.IsPlaying())
                scale.Kill();
            if (moveY != null && moveY.IsPlaying())
                moveY.Kill();
            if (opacity != null && opacity.IsPlaying())
                opacity.Kill();
            if (sequence!=null && sequence.IsPlaying())
            {
                sequence.Kill();
            }
            sequence = DOTween.Sequence();
            newCardCell = Instantiate(Resources.Load<GameObject>("BagSystem/Card"));
            newCardCell.transform.SetParent(GameObject.Find("Canvas").transform);
            newCardCell.transform.localScale = Vector2.zero;
            newCardCell.GetComponent<CanvasGroup>().alpha = 0;
            newCardCell.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -983f);

            int id = DXUtils.GetRandom(properList[curSelectProIndex]);
            var cc = cardList.Where(c => GetLevelValueByLevelStr(c.level) == id);

            newCardCell.GetComponent<CardCell>().Init(cc.ToList()[Random.Range(0, cc.ToList().Count)]);

            scale = newCardCell.transform.DOScale(1, 0.65f);
            moveY = newCardCell.transform.GetComponent<RectTransform>().DOAnchorPosY(0, 0.65f).SetEase(Ease.OutElastic);
            opacity = newCardCell.GetComponent<CanvasGroup>().DOFade(1, 0.65f).SetEase(Ease.Linear).OnComplete(() =>
            {
                newCardCell.GetComponent<CardCell>().canRot = true;
            });

            sequence.Join(moveY);
            sequence.Join(scale);
            sequence.Join(opacity);
        }
    }

    Tweener addTextScale;
    public void OnRewardGold(int resultGold)
    {
        if (addTextScale != null && addTextScale.IsPlaying()) {
            addTextScale.Kill();
            add_gold_label.gameObject.SetActive(false);
        }
        add_gold_label.gameObject.SetActive(true);
        add_gold_label.text = "+ " + resultGold;
        addTextScale = add_gold_label.transform.DOScale(Vector3.one * 1.7f, 0.2f).OnComplete(() => {
            add_gold_label.transform.DOScale(Vector3.one, 0.02f).SetDelay(0.4f).OnComplete(() => {
                add_gold_label.gameObject.SetActive(false);
            });
        });
    }
}