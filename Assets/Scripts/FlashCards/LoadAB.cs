using UnityEngine;
using System.Collections;

public class LoadAB : MonoBehaviour
{
    // AssetBundle的文件名
    public string bundleName = "monkeyking";
    // 资源名
    public string assetName = "monkeyking";

    void Start()
    {
        StartCoroutine(LoadAssetBundle());
    }

    IEnumerator LoadAssetBundle()
    {
        // AssetBundle的路径
        string bundlePath = Application.dataPath + "/Resources/" + bundleName;

        // 从文件加载AssetBundle
        AssetBundle bundle = AssetBundle.LoadFromFile(bundlePath);

        if (bundle == null)
        {
            Debug.LogError("Failed to load AssetBundle!");
            yield break;
        }

        //// 从AssetBundle中加载资源
        //AssetBundleRequest request = bundle.LoadAssetAsync<GameObject>(assetName);

        //yield return request;

        //if (request.asset != null)
        //{
        //    Instantiate(request.asset);
        //}
        //else
        //{
        //    Debug.LogError("Failed to load asset from AssetBundle!");
        //}

        //bundle.Unload(false);
    }
}
