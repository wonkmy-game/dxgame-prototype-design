﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 资源类型
/// </summary>
public enum ResType
{
    tree,
    stone,
    goldstone,
    chest
}
public enum TileType
{
    grass,
    dirt,
    water
}

public enum ToolType
{
    axe,
    pickaxe,
    shovel
}

/// <summary>
/// 玩家当前正在干的事儿
/// </summary>
public enum PlayerAction
{
    /// <summary>
    /// 什么也不做
    /// </summary>
    NULL,
    /// <summary>
    /// 收集资源
    /// </summary>
    COLLECT,
    /// <summary>
    /// 农事
    /// </summary>
    FARMING,
    /// <summary>
    /// 建造
    /// </summary>
    BUILD,
    /// <summary>
    /// 制作
    /// </summary>
    CRAFTING
}

[System.Serializable]
public struct ResData
{
    public int type;
    public Vector3Int coordinate;
    public Vector3 pos;

    public ResData(int type, Vector3Int coordinate, Vector3 pos)
    {
        this.type = type;
        this.coordinate = coordinate;
        this.pos = pos;
    }
}
[System.Serializable]
public class MapData
{
    public List<ResData> data;

    public MapData(List<ResData> _data)
    {
        data = new List<ResData>();
        foreach (ResData rd in _data)
        {
            data.Add(new ResData(rd.type, rd.coordinate, rd.pos));
        }
    }
}

[System.Serializable]
public class PlayerData
{
    public MapData mapData;
    public Inventory playerBag;
    public Inventory shopBag;
    public PlayerData(MapData mapData, Inventory playerBag, Inventory shopBag)
    {
        this.mapData = mapData;
        this.playerBag = playerBag;
        this.shopBag = shopBag;
    }
}
[System.Serializable]
public class ResCountData
{
    public int Woods;
    public int Stones;
    public int Golds;
    public int Foods;

    public ResCountData(int woods, int stones, int golds, int foods)
    {
        Woods = woods;
        Stones = stones;
        Golds = golds;
        Foods = foods;
    }

    public override string ToString()
    {
        return "woods," + Woods + "\nstones," + Stones + "\ngolds," + Golds + "\nfoods," + Foods;
    }
}

public delegate void OnResEntityDelete();