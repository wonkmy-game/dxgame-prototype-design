using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using DG.Tweening;
using UnityEngine.Events;

[System.Serializable]
public class MapManager
{
    Vector3 p;

    public Vector3Int iPos { get; set; }

    PlayerData playerData;

    List<ResEntity> allResEntityList;

    public List<ResData> resDatas;
    public string filePath { get; set; }

    private Transform frame;
    private Tilemap tilemap;
    public void Init()
    {
        frame = GameManager.instance.frame;
        tilemap = GameManager.instance.tilemap;
        frame.gameObject.SetActive(false);
        resDatas = new List<ResData>();
        allResEntityList = new List<ResEntity>();
    }
    public void StartGenerator(string command)
    {
        GameManager.instance.GameStart = true;
        frame.gameObject.SetActive(true);

        DXUtils.AddAplhaTransition(() =>
        {
            GameManager.instance.cicle_transition.GetChild(1).gameObject.SetActive(true);
            if (command.Equals("new_game"))
            {
                NewMapGen();
            }
            else if (command.Equals("load_game"))
            {
                LoadMapAndBagpack();
            }

            DXUtils.AddAplhaTransition(() =>
            {
                GameManager.instance.cicle_transition.GetChild(1).gameObject.SetActive(false);
            }, 0, 0.5f, 3.0f);
        }, 5, 0.5f, 0);

        GameManager.instance.gamingPanel.SetActive(true);

        GameManager.instance.mainPanel.SetActive(false);
    }

    void LoadMapAndBagpack()
    {
        if (DXUtils.IsHaveLocalStorage(filePath))
        {
            playerData = DXUtils.ReadMapDataFromJsonFile<PlayerData>(filePath, GameManager.instance.NeedEncryption);
            GameManager.instance.playerBag.data = playerData.playerBag.data;
            GameManager.instance.playerBag.name = playerData.playerBag.name;
            GameManager.instance.playerBag.TotalCapcity = playerData.playerBag.TotalCapcity;
            GameManager.instance.playerBagPanel = new InventoryPanel(playerData.playerBag, GameManager.instance.inventorysPanel, new Vector2(100, 100));

            GameManager.instance.shopBag.data = playerData.shopBag.data;
            GameManager.instance.shopBag.name = playerData.shopBag.name;
            GameManager.instance.shopBag.TotalCapcity = playerData.shopBag.TotalCapcity;
            GameManager.instance.shopBagPanel = new InventoryPanel(playerData.shopBag, GameManager.instance.inventorysPanel_world, new Vector2(100, 100));

            for (int i = 0; i < playerData.mapData.data.Count; i++)
            {
                GenResourceToGrass(playerData.mapData.data[i].type, playerData.mapData.data[i].coordinate, playerData.mapData.data[i].pos);
            }
        }
    }

    void NewMapGen()
    {
        BoundsInt bounds = tilemap.cellBounds;

        foreach (Vector3Int pos in bounds.allPositionsWithin)
        {
            if (tilemap.HasTile(pos))
            {
                TileBase tb = tilemap.GetTile(pos);
                if (tb != null)
                {
                    if (tb.name.StartsWith(TileType.grass.ToString()))
                    {
                        GeneratorMapRes(TileType.grass, pos, pos + new Vector3(0.5f, 0.5f, 0));
                    }
                }
            }
        }

        GameManager.instance.SaveGame(resDatas);
    }
    public void Update()
    {
        p = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        p.z = 0;
        iPos = tilemap.WorldToCell(p);

        TileBase tb = tilemap.GetTile(iPos);//根据鼠标的坐标获取地块儿
        if (tb != null)
        {
            ShowTileFrame(iPos, tb);//显示地块儿的高亮框

            OnCheckClickTileOrEntity(iPos, tb);//检查点击的是地块儿还是各种资源，并执行相应的逻辑
        }
    }

    /// <summary>
    /// 显示地块儿的高亮框
    /// </summary>
    /// <param name="iPos"></param>
    /// <param name="tb"></param>
    private void ShowTileFrame(Vector3Int iPos, TileBase tb)
    {
        if (tb.name.Equals(TileType.water.ToString()))
        {
            frame.gameObject.SetActive(false);
        }
        else
        {
            frame.gameObject.SetActive(true);
        }
        frame.transform.position = iPos + new Vector3(0.5f, 0.5f, 0);
    }
    /// <summary>
    /// 检查点击的是地块儿还是各种资源，并执行相应的逻辑
    /// </summary>
    /// <param name="iPos"></param>
    /// <param name="tb"></param>
    private void OnCheckClickTileOrEntity(Vector3Int iPos, TileBase tb)
    {
        if (Input.GetMouseButtonDown(0)) {
            switch (GameManager.instance.CurPlayerAction)
            {
                case PlayerAction.NULL:
                    break;
                case PlayerAction.FARMING:
                    if (GameManager.instance.CurSelectedTooltype == ToolType.shovel)
                    {
                        if (tb.name.Equals(TileType.grass.ToString()) || tb.name.Equals(TileType.dirt.ToString()))
                        {
                            Debug.Log("可以犁地>_-");
                        }
                    }
                    break;
                case PlayerAction.COLLECT:
                    for (int i = 0; i < allResEntityList.Count; i++)
                    {
                        if (allResEntityList[i].coordinate.Equals(new Vector2Int(iPos.x, iPos.y)))
                        {
                            switch (allResEntityList[i].resType)
                            {
                                case ResType.tree:
                                    if (GameManager.instance.CurSelectedTooltype == ToolType.axe)
                                    {
                                        var removedItem = allResEntityList[i];
                                        removedItem.Damage(10, () =>
                                        {
                                            allResEntityList.RemoveAt(i);
                                            resDatas.RemoveAt(i);
                                            DXDebug.TextPopup("wood +1", iPos);
                                            GameManager.instance.AddWood(1);
                                            Object.Destroy(removedItem.gameObject);
                                        });
                                    }
                                    break;
                                case ResType.stone:
                                    if (GameManager.instance.CurSelectedTooltype == ToolType.pickaxe)
                                    {
                                        var removedItem = allResEntityList[i];
                                        removedItem.Damage(10, () =>
                                        {
                                            allResEntityList.RemoveAt(i);
                                            resDatas.RemoveAt(i);
                                            DXDebug.TextPopup("stone +1", iPos);
                                            GameManager.instance.AddStone(1);
                                            Object.Destroy(removedItem.gameObject);
                                        });
                                    }
                                    break;
                                case ResType.goldstone:
                                    if (GameManager.instance.CurSelectedTooltype == ToolType.pickaxe)
                                    {
                                        var removedItem = allResEntityList[i];
                                        removedItem.Damage(10, () =>
                                        {
                                            allResEntityList.RemoveAt(i);
                                            resDatas.RemoveAt(i);
                                            DXDebug.TextPopup("gold stone +1", iPos);
                                            GameManager.instance.AddGold(1);
                                            Object.Destroy(removedItem.gameObject);
                                        });
                                    }
                                    break;
                                case ResType.chest:
                                    break;
                            }
                        }
                    }
                    break;
                case PlayerAction.BUILD:
                    GameObject newChest = Object.Instantiate(Resources.Load<GameObject>("chest"));
                    newChest.transform.position = tilemap.CellToWorld(iPos) + new Vector3(0.5f, 0.5f, 0);
                    ResEntity entity_chest = newChest.GetComponent<ResEntity>();
                    entity_chest.Init(ResType.chest, new Vector2Int(iPos.x, iPos.y), false);
                    allResEntityList.Add(entity_chest);
                    resDatas.Add(new ResData((int)entity_chest.resType, iPos, newChest.transform.position));
                    break;
                case PlayerAction.CRAFTING:
                    break;
            }
            GameManager.instance.SaveGame(resDatas);
        }
    }

    /// <summary>
    /// 生成地图上的各种资源
    /// </summary>
    private void GeneratorMapRes(TileType tileType,Vector3Int coordinate, Vector3 pos) {
        switch (tileType)
        {
            case TileType.grass:
                GenResourceToGrass(coordinate,pos);
                break;
            case TileType.dirt:
                break;
            case TileType.water:
                break;
            default:
                break;
        }
    }

    private void GenResourceToGrass(int type, Vector3Int coordinate,Vector3 pos)
    {
        GameObject newRes = Object.Instantiate(Resources.Load<GameObject>(((ResType)type).ToString()));
        newRes.transform.position = pos;
        resDatas.Add(new ResData(type, coordinate, newRes.transform.position));
        if (type != (int)ResType.chest)
        {
            newRes.transform.localScale *= type == 0 ? 0.23f : 0.41f;
            newRes.GetComponent<ResEntity>().Init((ResType)type, new Vector2Int(coordinate.x, coordinate.y), true);
        }
        else
        {
            newRes.GetComponent<ResEntity>().Init((ResType)type,new Vector2Int(coordinate.x, coordinate.y),false);
        }
        allResEntityList.Add(newRes.GetComponent<ResEntity>());
    }
    private void GenResourceToGrass(Vector3Int coordinate, Vector3 pos)
    {
        int rand = Random.Range(0, 100);
        
        float[] probabilities = { 0.4f, 0.3f, 0.1f };//概率数组，数组长度根据id的数量确定,这里的情况则是：0.4代表树木的生成概率，0.3代表普通石头,0.1代表金矿
        int type = DXUtils.GetRandom(probabilities);

        //int ra = DXUtils.GetRandom(new int[] { 40, 30, 10 }, 100);
        if (rand >= 70)
        {
            GameObject newRes = Object.Instantiate(Resources.Load<GameObject>(((ResType)type).ToString()));
            newRes.transform.position = pos;
            newRes.transform.localScale *= type == 0 ? 0.23f : 0.41f;
            resDatas.Add(new ResData(type, coordinate, newRes.transform.position));
            newRes.GetComponent<ResEntity>().Init((ResType)type, new Vector2Int(coordinate.x, coordinate.y),true);
            allResEntityList.Add(newRes.GetComponent<ResEntity>());
        }
    }
}