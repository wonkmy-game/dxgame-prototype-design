using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ResSpriteLoader : MonoBehaviour
{
    public List<Sprite> allSprites;
    public Sprite GetSprite()
    {
        int rd = Random.Range(0, allSprites.Count);
        return allSprites[rd];
    }
}
