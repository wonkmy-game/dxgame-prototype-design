using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public bool GameStart { get; set; }
    public MapManager mapManager;

    public Inventory playerBag;
    public InventoryPanel playerBagPanel;

    public Inventory shopBag;
    public InventoryPanel shopBagPanel;
    public string filePath { get; set; }
    /// <summary>
    /// 存档是否需要加密
    /// </summary>
    public bool NeedEncryption { get; set; }

    public InputField createNewGame;
    public Button newgameBtn;
    public Button loadgameBtn;
    public GameObject fileListPanel;

    public Tilemap tilemap;

    public Transform frame;
    public Transform fileCellParent;
    public Transform toolsCellParent;
    public Transform inventorysPanel;

    public Transform inventorysPanel_world;

    public Transform cicle_transition;

    public GameObject mainPanel;
    public GameObject settingPanel;
    public GameObject gamingPanel;

    public ToolType CurSelectedTooltype;
    int curToolTypeIndex = 0;
    List<Transform> allToolTypeList;

    public PlayerAction CurPlayerAction;

    public string folderPathLeftPart = "";
    public string curFolderName;//当前存档的文件夹名

    //游戏中的资源存档变量
    public int Woods { get; set; }
    public int Stones { get; set; }
    public int Golds { get; set; }
    public int Foods { get; set; }

    float fov = 60;
    private void Awake()
    {
        instance = this;
    }

    public void Init()
    {
        GameStart = false;
        NeedEncryption = false;

        folderPathLeftPart= Application.dataPath + "/GameDatas/";
        GeneratorToolFile();
        allToolTypeList = new List<Transform>();
        fileListPanel.SetActive(false);
        settingPanel.SetActive(false);
        mapManager = new MapManager();
        mapManager.Init();

        CurPlayerAction = PlayerAction.COLLECT;//默认为收集状态

        newgameBtn.onClick.AddListener(CreateNewGame);
        loadgameBtn.onClick.AddListener(LoadGame);
        GenaratorToolsFromFile();

        Debug.Log(Mathf.Tan(fov / 360.0f * Mathf.PI));
    }

    void TestConstructShopInventory()
    {
        shopBag = InventoryFactory.CreateInventory("shopBag", 12);
        //shopBag.AddItem(new Item(0, "wood", "a wood", 10, 64));
        shopBagPanel = new InventoryPanel(shopBag, inventorysPanel_world,new Vector2(100,100));
    }
    void GeneratorToolFile()
    {
        DXUtils.WriteFile(Application.dataPath + "/GameDatas/alltools.txt", "axe\npickaxe\nshovel", true, NeedEncryption);
    }
    void GenaratorToolsFromFile()
    {
        DXUtils.ReadFile(Application.dataPath + "/GameDatas/alltools.txt", NeedEncryption,(data) => {
            GameObject newToolCell = Instantiate(Resources.Load<GameObject>("toolCell"));
            newToolCell.transform.SetParent(toolsCellParent);
            ToolType type = DXUtils.GetEnumFromString<ToolType>(data);
            newToolCell.GetComponent<ToolCell>().Init(type, "imgs/" + data);
            allToolTypeList.Add(newToolCell.transform);
        });
        SwitchToolType(curToolTypeIndex);
    }

    public void ChangeCurToolTypeWithMouseMiddle(int num)
    {
        curToolTypeIndex += num;
        if (curToolTypeIndex > allToolTypeList.Count - 1)
        {
            curToolTypeIndex = 0;
        }
        if (curToolTypeIndex < 0)
        {
            curToolTypeIndex = allToolTypeList.Count - 1;
        }
        SwitchToolType(curToolTypeIndex);
    }

    void SwitchToolType(int index)
    {
        CurSelectedTooltype = allToolTypeList[index].GetComponent<ToolCell>().toolType;
        for (int i = 0; i < allToolTypeList.Count; i++)
        {
            allToolTypeList[i].Find("frame").gameObject.SetActive(false);
        }
        DXUtilsStatic.CreateTextPopupNoAnimation("当前选择了:" + CurSelectedTooltype.ToString(), Vector3.zero);
        allToolTypeList[index].Find("frame").gameObject.SetActive(true);
    }
    private void Update()
    {
        if (GameStart)
        {
            if (mapManager != null)
            {
                mapManager.Update();
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                settingPanel.SetActive(true);
            }

            if (Input.GetKeyDown(KeyCode.T))
            {
                //playerBag.AddItem(new Item(0, "wood", "a wood", 1, 64));
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                if (inventorysPanel.gameObject.activeSelf)
                {
                    playerBagPanel.DeleteAllGUI();
                    inventorysPanel.gameObject.SetActive(false);
                }
                else
                {
                    inventorysPanel.gameObject.SetActive(true);
                    playerBagPanel.UpdateData(Vector3Int.zero,false);
                }
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                if (inventorysPanel_world.gameObject.activeSelf)
                {
                    shopBagPanel.DeleteAllGUI();
                    inventorysPanel_world.gameObject.SetActive(false);
                }
                else
                {
                    inventorysPanel_world.gameObject.SetActive(true);

                    shopBagPanel.UpdateData(mapManager.iPos,false);
                }
            }

            float scw = Input.GetAxis("Mouse ScrollWheel");
            if (scw < 0)
            {
                ChangeCurToolTypeWithMouseMiddle(-1);
            }
            if (scw > 0)
            {
                ChangeCurToolTypeWithMouseMiddle(1);
            }
        }
    }
    #region 创建或加载游戏数据
    public void CreateNewGame() {
        string folderPath = DXUtils.CreateNewFolder(createNewGame.text);
        curFolderName = createNewGame.text;
        filePath = string.Format(folderPath + "/{0}.json", createNewGame.text != "" ? createNewGame.text : "default_game"+System.DateTime.Now.ToString());

        //为新存档创建一个背包
        playerBag = InventoryFactory.CreateInventory("playerBag", 25);
        playerBagPanel = new InventoryPanel(playerBag,inventorysPanel, new Vector2(100, 100));

        TestConstructShopInventory();

        DXUtils.WriteFile(Application.dataPath + "/GameDatas/saves.txt", createNewGame.text, NeedEncryption);
        mapManager.StartGenerator("new_game");
    }
    public void LoadGame()
    {
        //先展示所有已存在的存档，再根据玩家选择的存档加载
        fileListPanel.SetActive(true);
        loadgameBtn.interactable = false;
        DXUtils.ReadFile(Application.dataPath + "/GameDatas/saves.txt", NeedEncryption, (data) => {
            GameObject newFilecell = Instantiate(Resources.Load<GameObject>("fileCell"));
            newFilecell.transform.SetParent(fileCellParent);
            newFilecell.GetComponent<FileCell>().Init(Application.dataPath + "/GameDatas/" + data, data);
        });
    }
    #endregion
    #region 保存各种游戏数据
    public void SaveGame()
    {
        SaveGame(mapManager.resDatas);
    }
    public void SaveGame(List<ResData> resDatas)
    {
        MapData mapData = new MapData(resDatas);
        SavePlayerData(mapData);
    }
    void SavePlayerData(MapData mapData) {
        PlayerData playerData = new PlayerData(mapData, playerBag,shopBag);
        DXUtils.WriteMapDataToJsonFile(playerData, filePath, NeedEncryption);
    }
    public ResCountData GetResData()
    {
        ResCountData oldData = null;
        if (PlayerPrefs.HasKey(curFolderName))
        {
            string str1 = PlayerPrefs.GetString(curFolderName);
            oldData = JsonUtility.FromJson<ResCountData>(str1);
        }else
        {
            oldData = new ResCountData(0, 0, 0, 0);
        }
        Woods = oldData.Woods;
        Stones = oldData.Stones;
        Golds = oldData.Golds;
        Foods = oldData.Foods;
        return oldData;
    }
    public void AddWood(int num)
    {
        ResCountData data = new ResCountData(num, 0, 0, 0);
        data.Woods = num;
        playerBag.AddItem(new Card(0, "wood", "", "a wood", num, new int[] { 0, 0 }));
        SavePlayerResCount(data);
    }
    public void AddStone(int num)
    {
        ResCountData data = new ResCountData(0, num, 0, 0);
        data.Stones = num;
        playerBag.AddItem(new Card(1, "stone", "", "a stone", num, new int[] { 0, 0 }));
        SavePlayerResCount(data);
    }
    public void AddGold(int num)
    {
        ResCountData data = new ResCountData(0, 0, num, 0);
        data.Golds = num;
        playerBag.AddItem(new Card(2, "gold", "", "a gold", num, new int[] { 0, 0 }));
        SavePlayerResCount(data);
    }
    public void AddFoods(int num)
    {
        ResCountData data = new ResCountData(0, 0, 0, num);
        data.Foods = num;
        playerBag.AddItem(new Card(3, "food", "", "a food", num, new int[] { 0, 0 }));
        SavePlayerResCount(data);
    }
    /// <summary>
    /// 保存游戏数据存档
    /// </summary>
    public void SavePlayerResCount(ResCountData newData)
    {
        if (PlayerPrefs.HasKey(curFolderName))
        {
            string str1 = PlayerPrefs.GetString(curFolderName);
            ResCountData oldData = JsonUtility.FromJson<ResCountData>(str1);
            oldData.Woods += newData.Woods;
            oldData.Stones += newData.Stones;
            oldData.Golds += newData.Golds;
            oldData.Foods += newData.Foods;
            string res_count_data = JsonUtility.ToJson(oldData);
            PlayerPrefs.SetString(curFolderName, res_count_data);
            gamingPanel.GetComponent<GameingPanel>().UpgradeAllREsLabel(oldData);
        }
        else
        {
            ResCountData data = newData;
            string res_count_data = JsonUtility.ToJson(data);
            PlayerPrefs.SetString(curFolderName, res_count_data);

            gamingPanel.GetComponent<GameingPanel>().UpgradeAllREsLabel(data);
        }
    }
    #endregion
}
