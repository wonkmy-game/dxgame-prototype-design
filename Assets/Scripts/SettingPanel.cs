using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingPanel : MonoBehaviour
{
    public Toggle fullScreentoggle;

    public AudioSource bgmAudioSource;
    public Slider musicSlider;

    public Button resolutionLeft_btn;
    public Button resolutionRight_btn;
    int curResIndex = 0;

    public Text LanText;
    public Text ChinaText;
    public Text EnglishText;

    public Text setText;
    public Text resolotionText;
    public Text fullScreenText;

    public Text backTomenuText;
    public Text quitGameText;

    public List<Vector2Int> supportResolutionList;
    private void Start()
    {
        supportResolutionList.Add(new Vector2Int(1280, 720));
        supportResolutionList.Add(new Vector2Int(1920,1080));
        supportResolutionList.Add(new Vector2Int(2560, 1440));

        fullScreentoggle.onValueChanged.AddListener(ToggleFullScreen);
        musicSlider.onValueChanged.AddListener(OnValueChange);

        bgmAudioSource.volume = musicSlider.value;

        resolutionLeft_btn.onClick.AddListener(()=> {
            ChangeResolution(-1);
        });
        resolutionRight_btn.onClick.AddListener(()=> {
            ChangeResolution(1);
        });
    }
    void ChangeResolution(int n)
    {
        curResIndex += n;
        if (curResIndex > supportResolutionList.Count - 1)
        {
            curResIndex = 0;
        }
        if (curResIndex < 0)
        {
            curResIndex = supportResolutionList.Count - 1;
        }
        resolotionText.text = supportResolutionList[curResIndex].ToString();

        SetScreenSize(supportResolutionList[curResIndex].x, supportResolutionList[curResIndex].y);
    }
    void OnValueChange(float f)
    {
        bgmAudioSource.volume = f;
    }
    public void SetScreenSize(int width, int height)
    {
        Screen.SetResolution(width, height, Screen.fullScreen);
    }

    void ToggleFullScreen(bool isOn)
    {
        if (!isOn)
        {
            SetScreenSize(1920, 1080);
        }
        SetFullScreen(isOn);
    }
    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    public void BackToMenu()
    {
        
    }
    public void ChangeLanguage(string lan)
    {
        
    }
}
